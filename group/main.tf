###############################################################################
# Required Variables
###############################################################################

variable "name" {
  type        = string
  description = "Name of the CloudFlare Access group."
}

variable "parent" {
  type        = string
  description = "The parent identifier. This can be either an account ID, a zone ID or zone name depending on the values of 'account' and 'zone_lookup'."
}

variable "include" {
  type = list(object({
    ip   = optional(list(string))
    geo  = optional(list(string))
    okta = optional(object({
      name                 = list(string)
      identity_provider_id = string
    }))
    saml = optional(object({
      attribute_name       = string
      attribute_value      = string
      identity_provider_id = string
    }))
    azure = optional(object({
      id                   = list(string)
      identity_provider_id = string
    }))
    email  = optional(list(string))
    group  = optional(list(string))
    github = optional(object({
      name                 = string
      teams                = list(string)
      identity_provider_id = string
    }))
    gsuite = optional(object({
      email                = list(string)
      identity_provider_id = string
    }))
    everyone                = optional(bool)
    auth_method             = optional(string)
    certificate             = optional(bool)
    common_name             = optional(string)
    email_domain            = optional(list(string))
    login_method            = optional(list(string))
    service_token           = optional(list(string))
    device_posture          = optional(list(string))
    any_valid_service_token = optional(bool)
  }))
  description = "A series of access conditions for including into the group."

  validation {
    condition = alltrue([
      for include in var.include: anytrue([
        for key in [
          "ip",
          "geo",
          "okta",
          "saml",
          "azure",
          "email",
          "group",
          "github",
          "gsuite",
          "everyone",
          "auth_method",
          "certificate",
          "common_name",
          "email_domain",
          "login_method",
          "service_token",
          "device_posture",
          "any_valid_service_token"
        ] : contains(keys(include), key) ? include[key] != null : false
      ])
    ])
    error_message = "Each include must have at least one rule."
  }
}

###############################################################################
# Optional Variables
###############################################################################

variable "account" {
  type        = bool
  default     = false
  description = "Should the group be added to an account ('true') or DNS zone ('false')?"
}

variable "exclude" {
  type = list(object({
    ip   = optional(list(string))
    geo  = optional(list(string))
    okta = optional(object({
      name                 = list(string)
      identity_provider_id = string
    }))
    saml = optional(object({
      attribute_name       = string
      attribute_value      = string
      identity_provider_id = string
    }))
    azure = optional(object({
      id                   = list(string)
      identity_provider_id = string
    }))
    email  = optional(list(string))
    group  = optional(list(string))
    github = optional(object({
      name                 = string
      teams                = list(string)
      identity_provider_id = string
    }))
    gsuite = optional(object({
      email                = list(string)
      identity_provider_id = string
    }))
    everyone                = optional(bool)
    auth_method             = optional(string)
    certificate             = optional(bool)
    common_name             = optional(string)
    email_domain            = optional(list(string))
    login_method            = optional(list(string))
    service_token           = optional(list(string))
    device_posture          = optional(list(string))
    any_valid_service_token = optional(bool)
  }))
  default     = []
  description = "A series of access conditions for excluding from the group."
}

variable "require" {
  type = list(object({
    ip   = optional(list(string))
    geo  = optional(list(string))
    okta = optional(object({
      name                 = list(string)
      identity_provider_id = string
    }))
    saml = optional(object({
      attribute_name       = string
      attribute_value      = string
      identity_provider_id = string
    }))
    azure = optional(object({
      id                   = list(string)
      identity_provider_id = string
    }))
    email  = optional(list(string))
    group  = optional(list(string))
    github = optional(object({
      name                 = string
      teams                = list(string)
      identity_provider_id = string
    }))
    gsuite = optional(object({
      email                = list(string)
      identity_provider_id = string
    }))
    everyone                = optional(bool)
    auth_method             = optional(string)
    certificate             = optional(bool)
    common_name             = optional(string)
    email_domain            = optional(list(string))
    login_method            = optional(list(string))
    service_token           = optional(list(string))
    device_posture          = optional(list(string))
    any_valid_service_token = optional(bool)
  }))
  default     = []
  description = "A series of access conditions that are required for the group."
}

###############################################################################
# Locals
###############################################################################

locals {
  account_id = var.account ? var.parent : null
}

###############################################################################
# Resources
###############################################################################

resource "cloudflare_access_group" "object" {
  name       = var.name
  zone_id    = local.zone_id
  account_id = local.account_id

  dynamic "exclude" {
    for_each = var.exclude

    content {
      ip                      = exclude.value["ip"]
      geo                     = exclude.value["geo"]
      email                   = exclude.value["email"]
      group                   = exclude.value["group"]
      everyone                = exclude.value["everyone"]
      auth_method             = exclude.value["auth_method"]
      certificate             = exclude.value["certificate"]
      common_name             = exclude.value["common_name"]
      email_domain            = exclude.value["email_domain"]
      login_method            = exclude.value["login_method"]
      service_token           = exclude.value["service_token"]
      device_posture          = exclude.value["device_posture"]
      any_valid_service_token = exclude.value["any_valid_service_token"]

      dynamic "okta" {
        for_each = exclude.value["okta"] == null ? [] : tolist([ exclude.value["okta"] ])

        content {
          name                 = okta.value["name"]
          identity_provider_id = okta.value["identity_provider_id"]
        }
      }


      dynamic "saml" {
        for_each = exclude.value["saml"] == null ? [] : tolist([ exclude.value["saml"] ])

        content {
          attribute_name       = saml.value["attribute_name"]
          attribute_value      = saml.value["attribute_value"]
          identity_provider_id = saml.value["identity_provider_id"]
        }
      }

      dynamic "azure" {
        for_each = exclude.value["azure"] == null ? [] : tolist([ exclude.value["azure"] ])

        content {
          id                   = azure.value["id"]
          identity_provider_id = azure.value["identity_provider_id"]
        }
      }

      dynamic "github" {
        for_each = exclude.value["github"] == null ? [] : tolist([ exclude.value["github"] ])

        content {
          name                 = github.value["name"]
          teams                = github.value["teams"]
          identity_provider_id = github.value["identity_provider_id"]
        }
      }

      dynamic "gsuite" {
        for_each = exclude.value["gsuite"] == null ? [] : tolist([ exclude.value["gsuite"] ])

        content {
          email                = gsuite.value["email"]
          identity_provider_id = gsuite.value["identity_provider_id"]
        }
      }
    }
  }

  dynamic "include" {
    for_each = var.include

    content {
      ip                      = include.value["ip"]
      geo                     = include.value["geo"]
      email                   = include.value["email"]
      group                   = include.value["group"]
      everyone                = include.value["everyone"]
      auth_method             = include.value["auth_method"]
      certificate             = include.value["certificate"]
      common_name             = include.value["common_name"]
      email_domain            = include.value["email_domain"]
      login_method            = include.value["login_method"]
      service_token           = include.value["service_token"]
      device_posture          = include.value["device_posture"]
      any_valid_service_token = include.value["any_valid_service_token"]

      dynamic "okta" {
        for_each = include.value["okta"] == null ? [] : tolist([ include.value["okta"] ])

        content {
          name                 = okta.value["name"]
          identity_provider_id = okta.value["identity_provider_id"]
        }
      }

      dynamic "saml" {
        for_each = include.value["saml"] == null ? [] : tolist([ include.value["saml"] ])

        content {
          attribute_name       = saml.value["attribute_name"]
          attribute_value      = saml.value["attribute_value"]
          identity_provider_id = saml.value["identity_provider_id"]
        }
      }

      dynamic "azure" {
        for_each = include.value["azure"] == null ? [] : tolist([ include.value["azure"] ])

        content {
          id                   = azure.value["id"]
          identity_provider_id = azure.value["identity_provider_id"]
        }
      }

      dynamic "github" {
        for_each = include.value["github"] == null ? [] : tolist([ include.value["github"] ])

        content {
          name                 = github.value["name"]
          teams                = github.value["teams"]
          identity_provider_id = github.value["identity_provider_id"]
        }
      }

      dynamic "gsuite" {
        for_each = include.value["gsuite"] == null ? [] : tolist([ include.value["gsuite"] ])

        content {
          email                = gsuite.value["email"]
          identity_provider_id = gsuite.value["identity_provider_id"]
        }
      }
    }
  }

  dynamic "require" {
    for_each = var.require

    content {
      ip                      = require.value["ip"]
      geo                     = require.value["geo"]
      email                   = require.value["email"]
      group                   = require.value["group"]
      everyone                = require.value["everyone"]
      auth_method             = require.value["auth_method"]
      certificate             = require.value["certificate"]
      common_name             = require.value["common_name"]
      email_domain            = require.value["email_domain"]
      login_method            = require.value["login_method"]
      service_token           = require.value["service_token"]
      device_posture          = require.value["device_posture"]
      any_valid_service_token = require.value["any_valid_service_token"]

      dynamic "okta" {
        for_each = require.value["okta"] == null ? [] : tolist([ require.value["okta"] ])

        content {
          name                 = okta.value["name"]
          identity_provider_id = okta.value["identity_provider_id"]
        }
      }


      dynamic "saml" {
        for_each = require.value["saml"] == null ? [] : tolist([ require.value["saml"] ])

        content {
          attribute_name       = saml.value["attribute_name"]
          attribute_value      = saml.value["attribute_value"]
          identity_provider_id = saml.value["identity_provider_id"]
        }
      }

      dynamic "azure" {
        for_each = require.value["azure"] == null ? [] : tolist([ require.value["azure"] ])

        content {
          id                   = azure.value["id"]
          identity_provider_id = azure.value["identity_provider_id"]
        }
      }

      dynamic "github" {
        for_each = require.value["github"] == null ? [] : tolist([ require.value["github"] ])

        content {
          name                 = github.value["name"]
          teams                = github.value["teams"]
          identity_provider_id = github.value["identity_provider_id"]
        }
      }

      dynamic "gsuite" {
        for_each = require.value["gsuite"] == null ? [] : tolist([ require.value["gsuite"] ])

        content {
          email                = gsuite.value["email"]
          identity_provider_id = gsuite.value["identity_provider_id"]
        }
      }
    }
  }
}

###############################################################################
# Outputs
###############################################################################

output "include" {
  value = var.include
}

###############################################################################

output "exclude" {
  value = var.exclude
}

output "require" {
  value = var.require
}

###############################################################################

output "account_id" {
  value = local.account_id
}

###############################################################################

output "id" {
  value = cloudflare_access_group.object.id
}

output "name" {
  value = cloudflare_access_group.object.name
}

###############################################################################
