###############################################################################
# Terraform Settings
###############################################################################

terraform {
  experiments = [
    module_variable_optional_attrs
  ]

  required_providers {
    cloudflare = {
      source = "cloudflare/cloudflare"
    }
  }
}

###############################################################################
