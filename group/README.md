<!---------------------------------------------------------------------------->

# group

#### Manage access groups within [Cloudflare for Teams]

-------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/identity/cloudflare//group`**

-------------------------------------------------------------------------------

### Example Usage

```
module "my_cloudflare_access_group" {
  source  = "gitlab.com/bitservices/identity/cloudflare//group"
  name    = "devops"
  parent  = "bitservices.io"
  include = [{
    "email_domain" = [ "bitservices.io" ]
  }]
}
```

<!---------------------------------------------------------------------------->

[Cloudflare for Teams]: https://www.cloudflare.com/teams/

<!---------------------------------------------------------------------------->
