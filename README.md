<!---------------------------------------------------------------------------->

# identity (cloudflare)

<!---------------------------------------------------------------------------->

## Description

Manage identity resources for [Cloudflare for Teams].

<!---------------------------------------------------------------------------->

## Modules

* [application](application/README.md) - Manage access applications and policies within [Cloudflare for Teams].
* [group](group/README.md) - Manage access groups within [Cloudflare for Teams].

<!---------------------------------------------------------------------------->

[Cloudflare for Teams]: https://www.cloudflare.com/teams/

<!---------------------------------------------------------------------------->
