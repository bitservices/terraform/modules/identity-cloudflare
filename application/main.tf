###############################################################################
# Required Variables
###############################################################################

variable "name" {
  type        = string
  description = "Name of the CloudFlare Access application."
}

variable "domain" {
  type        = string
  description = "Domain the application to be protected by CloudFlare Access."
}

variable "parent" {
  type        = string
  description = "The parent identifier. This can be either an account ID, a zone ID or zone name depending on the values of 'account' and 'zone_lookup'."
}

###############################################################################
# Optional Variables
###############################################################################

variable "auto" {
  type        = bool
  default     = true
  description = "Automatically pick identity provider if only one is configured."
}

variable "type" {
  type        = string
  default     = "self_hosted"
  description = "The application type. Valid values are 'self_hosted', 'ssh', 'vnc', or 'file'."
}

variable "cookie" {
  type        = bool
  default     = false
  description = "Option to provide increased security against compromised authorization tokens and CSRF attacks by requiring an additional binding cookie on requests."
}

variable "account" {
  type        = bool
  default     = false
  description = "Should the application be added to an account ('true') or DNS zone ('false')?"
}

variable "duration" {
  type        = string
  default     = "1h"
  description = "How long should a session remain valid before the user needs to re-authorise."
}

###############################################################################

variable "deny_url" {
  type        = string
  default     = null
  description = "Option that redirects to a custom URL when a user is denied access to the application."
}

variable "deny_message" {
  type        = string
  default     = null
  description = "Option that returns a custom error message when a user is denied access to the application."
}

###############################################################################
# Locals
###############################################################################

locals {
  account_id = var.account ? var.parent : null
}

###############################################################################
# Resources
###############################################################################

resource "cloudflare_access_application" "object" {
  name                      = var.name
  type                      = var.type
  domain                    = var.domain
  zone_id                   = local.zone_id
  account_id                = local.account_id
  custom_deny_url           = var.deny_url
  session_duration          = var.duration
  custom_deny_message       = var.deny_message
  enable_binding_cookie     = var.cookie
  auto_redirect_to_identity = var.auto
}

###############################################################################
# Outputs
###############################################################################

output "type" {
  value = var.type
}

output "cookie" {
  value = var.cookie
}

###############################################################################

output "deny_url" {
  value = var.deny_url
}

output "deny_message" {
  value = var.deny_message
}

###############################################################################

output "account_id" {
  value = local.account_id
}

###############################################################################

output "id" {
  value = cloudflare_access_application.object.id
}

output "aud" {
  value = cloudflare_access_application.object.aud
}

output "auto" {
  value = cloudflare_access_application.object.auto_redirect_to_identity
}

output "idps" {
  value = cloudflare_access_application.object.allowed_idps
}

output "name" {
  value = cloudflare_access_application.object.name
}

output "domain" {
  value = cloudflare_access_application.object.domain
}

output "duration" {
  value = cloudflare_access_application.object.session_duration
}

###############################################################################
