<!---------------------------------------------------------------------------->

# application

#### Manage access applications and policies within [Cloudflare for Teams]

-------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/identity/cloudflare//application`**

-------------------------------------------------------------------------------

### Example Usage

```
module "my_cloudflare_access_group" {
  source  = "gitlab.com/bitservices/identity/cloudflare//group"
  name    = "devops"
  parent  = "bitservices.io"
  include = [{
    "email_domain" = [ "bitservices.io" ]
  }]
}

module "my_cloudflare_access_application" {
  source         = "gitlab.com/bitservices/identity/cloudflare//application"
  name           = "foobar"
  domain         = "foobar.bitservices.io"
  parent         = "bitservices.io"
  policy_include = [ module.my_cloudflare_access_group.id ]
}
```

<!---------------------------------------------------------------------------->

[Cloudflare for Teams]: https://www.cloudflare.com/teams/

<!---------------------------------------------------------------------------->
