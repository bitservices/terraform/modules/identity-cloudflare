###############################################################################
# Required Variables
###############################################################################

variable "policy_include" {
  type        = list(string)
  description = "A list of access groups to be included by the access policy."
}

###############################################################################
# Optional Variables
###############################################################################

variable "policy_exclude" {
  type        = list(string)
  default     = []
  description = "A list of access groups to be excluded by the access policy."
}

variable "policy_require" {
  type        = list(string)
  default     = []
  description = "A list of access groups to be required by the access policy."
}

variable "policy_decision" {
  type        = string
  default     = "allow"
  description = "Defines the action Access will take if the policy matches the user. Allowed values: 'allow', 'deny', 'non_identity' or 'bypass'."
}

variable "policy_precedence" {
  type        = number
  default     = 1
  description = "The unique precedence for policies on a single application."
}

###############################################################################

variable "policy_justification_prompt" {
  type        = string
  default     = "Please enter reason for access"
  description = "String to present to the user when purpose justification is enabled."
}

variable "policy_justification_enabled" {
  type        = bool
  default     = false
  description = "Whether to prompt the user for a justification for accessing the application."
}

###############################################################################
# Locals
###############################################################################

locals {
  policy_name = format("%s-policy", var.name)
}

###############################################################################
# Resources
###############################################################################

resource "cloudflare_access_policy" "object" {
  name                           = local.policy_name
  zone_id                        = local.zone_id
  decision                       = var.policy_decision
  account_id                     = local.account_id
  precedence                     = var.policy_precedence
  application_id                 = cloudflare_access_application.object.id
  purpose_justification_prompt   = var.policy_justification_prompt
  purpose_justification_required = var.policy_justification_enabled

  dynamic "exclude" {
    for_each = length(var.policy_exclude) > 0 ? tolist([ var.policy_exclude ]) : []

    content {
      group = exclude.value
    }
  }

  include {
    group = var.policy_include
  }

  dynamic "require" {
    for_each = length(var.policy_require) > 0 ? tolist([ var.policy_require ]) : []

    content {
      group = require.value
    }
  }
}

###############################################################################
# Outputs
###############################################################################

output "policy_include" {
  value = var.policy_include
}

###############################################################################

output "policy_exclude" {
  value = var.policy_exclude
}

output "policy_require" {
  value = var.policy_require
}

output "policy_decision" {
  value = var.policy_decision
}

output "policy_precedence" {
  value = var.policy_precedence
}

###############################################################################

output "policy_justification_prompt" {
  value = var.policy_justification_prompt
}

output "policy_justification_enabled" {
  value = var.policy_justification_enabled
}

###############################################################################

output "policy_id" {
  value = cloudflare_access_policy.object.id
}

output "policy_name" {
  value = cloudflare_access_policy.object.name
}

###############################################################################
